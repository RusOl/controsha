﻿using System;
using System.Collections.Generic;
using System.Text;
using Blogs.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blogs.DAL.EntitiesConfiguration
{
    public class CommentConfiguration : BaseEntityConfiguration<Comment>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Comment> builder)
        {
            builder
                .Property(b => b.Content)
                .HasMaxLength(1000)
                .IsRequired();

            builder
                .Property(b => b.CreatedOn)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasOne(b => b.Author)
                .WithMany(b => b.Comments)
                .HasForeignKey(b => b.AuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(c => c.Ad)
                .WithMany(c => c.Comments)
                .HasForeignKey(c => c.AdId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
