﻿using Blogs.DAL.Entities;
using Blogs.DAL.EntitiesConfiguration.Contracts;

namespace Blogs.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Ad> AdConfiguration { get; }
        public IEntityConfiguration<Category> CategoryConfiguration { get; }
        public IEntityConfiguration<Comment> CommentConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            AdConfiguration = new AdConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            CommentConfiguration = new CommentConfiguration();
        }
    }
}
