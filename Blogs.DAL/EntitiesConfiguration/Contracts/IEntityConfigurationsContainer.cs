﻿using Blogs.DAL.Entities;

namespace Blogs.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Ad> AdConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<Comment> CommentConfiguration { get; }
    }
}