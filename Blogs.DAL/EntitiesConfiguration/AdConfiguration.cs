﻿using System;
using System.Collections.Generic;
using System.Text;
using Blogs.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blogs.DAL.EntitiesConfiguration
{
    public class AdConfiguration : BaseEntityConfiguration<Ad>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Ad> builder)
        {
            builder
                .Property(b => b.Title)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b => b.Contacts)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b => b.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(b => b.Price)
                .HasDefaultValue(0.00M)
                .IsRequired();
            builder
                .Property(b => b.DatePublished)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Ad> builder)
        {
            builder
                .HasOne(b => b.Category)
                .WithMany(b => b.Ads)
                .HasForeignKey(b => b.CategoryId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasMany(r => r.Comments)
                .WithOne(r => r.Ad)
                .HasForeignKey(r => r.AdId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasOne(b => b.User)
                .WithMany(b => b.Ads)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.Restrict) 
                .IsRequired();
        }
    }
}
