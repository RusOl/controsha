﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blogs.DAL.Entities
{
    public class Ad : IEntity
    {
        public int Id { get; set; }
        public DateTime DatePublished { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Content { get; set; }
        public string Contacts { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int Up { get; set; }
        public DateTime UpTime { get; set; }
        public byte[] Image { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
