﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blogs.DAL.Entities
{
    public class Comment : IEntity
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int AdId { get; set; }
        public Ad Ad { get; set; }
    }
}
