﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blogs.DAL.Entities;
using Blogs.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Blogs.DAL.Repositories
{
    public class AdRepository : Repository<Ad>, IAdRepository
    {
        public AdRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Ads;
        }

        public IEnumerable<Ad> GetAllWithAuthorsAndComments()
        {
            return entities
                .Include(e => e.User)
                .Include(e => e.Comments)
                .ThenInclude(c => c.Author)
                .ToList();
        }

        public Ad GetByIdWithAuthorsAndComments(int id)
        {
            return entities
                .Include(e => e.User)
                .Include(e => e.Comments)
                .ThenInclude(c => c.Author)
                .FirstOrDefault(c => c.Id == id);
        }
    }
}
