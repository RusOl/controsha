﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blogs.DAL.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
