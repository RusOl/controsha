﻿using System;
using System.Collections.Generic;
using System.Text;
using Blogs.DAL.Entities;

namespace Blogs.DAL.Repositories.Contracts
{
    public interface IAdRepository : IRepository<Ad>
    {
        IEnumerable<Ad> GetAllWithAuthorsAndComments();
        Ad GetByIdWithAuthorsAndComments(int id);
    }
}
