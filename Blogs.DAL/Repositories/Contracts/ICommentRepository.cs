﻿using System;
using System.Collections.Generic;
using System.Text;
using Blogs.DAL.Entities;

namespace Blogs.DAL.Repositories.Contracts
{
    public interface ICommentRepository : IRepository<Comment>
    {
    }
}
