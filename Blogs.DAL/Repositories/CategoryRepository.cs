﻿using System;
using System.Collections.Generic;
using System.Text;
using Blogs.DAL.Entities;
using Blogs.DAL.Repositories.Contracts;

namespace Blogs.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}
