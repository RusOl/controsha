﻿using System;
using Blogs.DAL.Repositories.Contracts;

namespace Blogs.DAL.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IAdRepository Ads { get; set; }
        public ICategoryRepository Category { get; set; }
        public ICommentRepository Comments { get; set; }
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Ads = new AdRepository(context);
            Category = new CategoryRepository(context);
            Comments = new CommentRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
