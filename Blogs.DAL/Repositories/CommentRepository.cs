﻿using System;
using System.Collections.Generic;
using System.Text;
using Blogs.DAL.Entities;
using Blogs.DAL.Repositories.Contracts;

namespace Blogs.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Comments;
        }
    }
}
