﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blogs.DAL
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
