﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Blogs.DAL.Entities;
using Blogs.DAL.Repositories;
using Blogs.Models;
using Blogs.Services;
using Blogs.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Blogs.Controllers
{
    public class AdController : Controller
    {
        private readonly IAdService _adService;
        private readonly UserManager<User> _userManager;
        

        public AdController(IAdService adService, UserManager<User> userManager)
        {
            _adService = adService;
            _userManager = userManager;
            
        }

        public IActionResult Index(AdIndexModel model)
        {
            try
            {
                var recordModels = _adService.GetAllAds(model);
                model.Ads = recordModels;
                

                return View(model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        public IActionResult Create()
        {
            var model = _adService.GetAdCreateModel();
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateRecord(AdCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);

                _adService.CreateAd(model, currentUser.Id);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Details(int recordId)
        {
            var recordModel = _adService.GetAdById(recordId);

            return View(recordModel);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddComment(AddCommentModel model)
        {
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);
                var commentModel = _adService.AddComment(model, currentUser);

                return Ok(commentModel);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Index()
        {
            try
            {
                AdIndexModel model = new AdIndexModel();
                model.UpTime = DateTime.Now;
                model.DateFrom = model.UpTime;
                var recordModels = _adService.GetAllAds(model);
                model.Ads = recordModels;
                return View(model);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        [AllowAnonymous]
        [HttpPost]
        public IActionResult UpAjax(int recordId)
        {
            try
            {
                var likes = _adService.AddUp(recordId);
                return Ok(likes);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
