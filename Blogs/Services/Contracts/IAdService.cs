﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogs.DAL.Entities;
using Blogs.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Blogs.Services.Contracts
{
    public interface IAdService
    {
        List<AdModel> GetAllAds(AdIndexModel model);
        void CreateAd(AdCreateModel model, int currentUserId);
        AdModel GetAdById(in int recordId);
        CommentModel AddComment(AddCommentModel model, User user);
        public AdCreateModel GetAdCreateModel();
        public SelectList GetCategoriesSelect();
        public int AddUp(int recordId);
    }
}
