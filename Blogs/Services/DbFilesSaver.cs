﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Blogs.DAL.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Blogs.Services
{
    public interface IFileSaver
    {
        void SaveFile(Ad record, IFormFile formFile);
    }

    public class DbFilesSaver : IFileSaver
    {
        public void SaveFile(Ad record, IFormFile formFile)
        {
            using (var binaryReader = new BinaryReader(formFile.OpenReadStream()))
            {
                record.Image = binaryReader.ReadBytes((int)formFile.Length);
            }
        }
    }
}
