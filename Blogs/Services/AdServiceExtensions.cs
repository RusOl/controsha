﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogs.DAL.Entities;

namespace Blogs.Services
{
    public static class AdServiceExtensions
    {
        public static IEnumerable<Ad> BySearchKey(this IEnumerable<Ad> ads, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                ads = ads.Where(r => r.Title.Contains(searchKey) || r.Content.Contains(searchKey));

            return ads;
        }

        public static IEnumerable<Ad> ByAuthorName(this IEnumerable<Ad> ads, string authorName)
        {
            if (!string.IsNullOrWhiteSpace(authorName))
                ads = ads.Where(r => r.User.UserName.Contains(authorName));

            return ads;
        }
        public static IEnumerable<Ad> ByCategory(this IEnumerable<Ad> records, string category)
        {
            if (!string.IsNullOrWhiteSpace(category))
                records = records.Where(r => r.User.UserName.Contains(category));

            return records;
        }
        public static IEnumerable<Ad> ByPriceFrom(this IEnumerable<Ad> records, decimal? PriceFrom)
        {
            if (PriceFrom.HasValue)
                records = records.Where(r => r.Price >= PriceFrom.Value);

            return records;
        }
        public static IEnumerable<Ad> ByPriceTo(this IEnumerable<Ad> records, decimal? dateTo)
        {
            if (dateTo.HasValue)
                records = records.Where(r => r.Price <= dateTo.Value);

            return records;
        }
        public static IEnumerable<Ad> ByDateFrom(this IEnumerable<Ad> ads, DateTime? dateFrom)
        {
            if (dateFrom.HasValue)
                ads = ads.Where(r => r.DatePublished >= dateFrom.Value);

            return ads;
        }

        public static IEnumerable<Ad> ByDateTo(this IEnumerable<Ad> ads, DateTime? dateTo)
        {
            if (dateTo.HasValue)
                ads = ads.Where(r => r.DatePublished <= dateTo.Value);

            return ads;
        }
    }
}
