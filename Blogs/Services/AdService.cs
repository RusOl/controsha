﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Blogs.DAL.Entities;
using Blogs.DAL.Repositories.Contracts;
using Blogs.Models;
using Blogs.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Blogs.Services
{
    public class AdService : IAdService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFileSaver _fileSaver;

        public AdService(IUnitOfWorkFactory unitOfWorkFactory, IFileSaver fileSaver)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _fileSaver = fileSaver;
        }

        public List<AdModel> GetAllAds(AdIndexModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var records = unitOfWork.Ads.GetAllWithAuthorsAndComments();
                records = records
                    .BySearchKey(model.SearchKey)
                    .ByAuthorName(model.Author)
                    .ByDateFrom(model.UpTime)
                    .ByDateTo(model.DateTo)
                    .ByCategory(model.Category)
                    .ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo);

                int pageSize = 2;
                int count = records.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                records = records.Skip((page - 1) * pageSize).Take(pageSize);
                model.PagingModel = new PagingModel(count, page, pageSize);
                model.Page = page;

                var models = Mapper.Map<List<AdModel>>(records.ToList());

                return models;
            }
        }
        public int AddUp(int recordId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var record = unitOfWork.Ads.GetById(recordId);
                record.Up++;
                unitOfWork.Ads.Update(record);
                return record.Up;
            }

        }

        public void CreateAd(AdCreateModel model, int currentUserId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var record = Mapper.Map<Ad>(model);
                record.UserId = currentUserId;

                _fileSaver.SaveFile(record, model.Image);

                unitOfWork.Ads.Create(record);
            }
        }

        public AdCreateModel GetAdCreateModel()
        {
            return new AdCreateModel()
            {
                CategoriesSelect = GetCategoriesSelect()
            };
        }

        public SelectList GetCategoriesSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Category.GetAll().ToList();
                return new SelectList(categories, nameof(Category.Id), nameof(Category.Name));
            }
        }

        public AdModel GetAdById(in int recordId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetByIdWithAuthorsAndComments(recordId);

                return Mapper.Map<AdModel>(ad);
            }
        }

        public CommentModel AddComment(AddCommentModel model, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var comment = Mapper.Map<Comment>(model);
                comment.AuthorId = user.Id;
                comment.CreatedOn = DateTime.Now;
                unitOfWork.Comments.CreateAsync(comment);
                comment.Author = user;
                return Mapper.Map<CommentModel>(comment);
            }
        }

    }
}
