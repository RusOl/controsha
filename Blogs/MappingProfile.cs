﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Blogs.DAL.Entities;
using Blogs.Models;

namespace Blogs
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateAdToAdModelMap();
            CreateAdCreateModelToAd();
            CreateCommentToCommentModelMap();
            CreateAddCommentRequestToCommentMap();
        }

        private void CreateAddCommentRequestToCommentMap()
        {
            CreateMap<AddCommentModel, Comment>()
                .ForMember(target => target.Content,
                    src => src.MapFrom(p => p.Comment));
        }

        private void CreateCommentToCommentModelMap()
        {
            CreateMap<Comment, CommentModel>()
                .ForMember(target => target.CreatedOn,
                    src => src.MapFrom(p => p.CreatedOn.ToString("D")))
                .ForMember(target => target.AuthorName,
                    src => src.MapFrom(p => p.Author.UserName));
        }

        public void CreateAdToAdModelMap()
        {
            CreateMap<Ad, AdModel>()
                .ForMember(target => target.DatePublished,
                    src => src.MapFrom(p => p.DatePublished.ToString("D")))
                .ForMember(target => target.Author,
                    src => src.MapFrom(p => p.User.UserName))
                .ForMember(target => target.ContentPreview,
                    src => src.MapFrom(p => p.Content.Substring(0, 20)));
        }

        public void CreateAdCreateModelToAd()
        {
            CreateMap<AdCreateModel, Ad>()
                .ForMember(target => target.DatePublished,
                    src => src.MapFrom(p => DateTime.Now))
                .ForMember(target => target.Image,
                    src => src.Ignore());
        }
    }
}
