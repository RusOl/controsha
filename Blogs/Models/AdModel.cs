﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blogs.Models
{
    public class AdModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата публикации")] 
        public string DatePublished { get; set; }

        [Display(Name = "Автор")] 
        public string Author { get; set; }

        [Display(Name = "Заголовок")] 
        public string Title { get; set; }

        [Display(Name = "Предпросмотр")] 
        public string ContentPreview { get; set; }

        [Display(Name = "Содержание")] 
        public string Content { get; set; }

        [Display(Name = "Цена")] 
        public decimal Price { get; set; }
        [Display(Name = "Контакты")]
        public string Contacts { get; set; }
        [Display(Name = "Boost")]
        public int Up { get; set; }
        public byte[] Image { get; set; }
        public string ImagePath { get; set; }
        public DateTime UpTime { get; set; }
        public List<CommentModel> Comments { get; set; }
    }
}
