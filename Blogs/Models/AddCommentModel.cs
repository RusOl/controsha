﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Blogs.Models
{
    public class AddCommentModel
    {
        [Required]
        [JsonProperty(propertyName: "comment")]
        public string Comment { get; set; }

        [Required]
        [JsonProperty(propertyName: "recordId")]
        public int RecordId { get; set; }
    }
}
