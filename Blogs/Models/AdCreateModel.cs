﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Blogs.Models
{
    public class AdCreateModel
    {
        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Содержание")]
        public string Content { get; set; }
        [Required]
        [Display(Name = "Категория")]
        public int CategoryId { get; set; }
        [Required]
        [Display(Name = "Цена")]
        public decimal Price { get; set; }
        [Required]
        [Display(Name = "Контакты")]
        public string Contacts { get; set; }

        [Required]
        [Display(Name = "Изображение")]
        public IFormFile Image { get; set; }
        public SelectList CategoriesSelect { get; set; }
    }
}
