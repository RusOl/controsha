﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blogs.Models
{
    public class CommentModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата публикации")]
        public string CreatedOn { get; set; }

        [Display(Name = "Автор")]
        public string AuthorName { get; set; }

        [Display(Name = "Содержание")]
        public string Content { get; set; }
    }
}
