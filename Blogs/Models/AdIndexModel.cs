﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blogs.Models
{
    public class AdIndexModel
    {
        public string Author { get; set; }
        public string SearchKey { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public List<AdModel> Ads { get; set; }
        public string Category { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }
        public int? Page { get; set; }
        public PagingModel PagingModel { get; set; }
        [Display(Name = "Кол-во boost'ов")]
        public int Up { get; set; }
        public DateTime UpTime { get; set; }
    }
}
